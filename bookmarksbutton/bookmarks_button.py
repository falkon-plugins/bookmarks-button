# ============================================================
# Bookmarks Button extension for Falkon
# Copyright (C) 2019 Zdravko Mitov <mitovz@mail.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ============================================================
import Falkon
from PySide2 import QtGui, QtCore


class BookmarksButton(Falkon.AbstractButtonInterface):

    def __init__(self):
        super().__init__()
        self.setIcon(QtGui.QIcon.fromTheme("bookmarks"))
        self.setTitle("Bookmarks Button")
        self.setToolTip("Call a bookmark")

        self.clicked.connect(self.on_clicked)

    def id(self):
        return "bookmarks-button"

    def name(self):
        return "Bookmarks button"

    def on_act_triggered(self, url):
        self.webView().load(url)

    def on_act_ctrl_triggered(self, url):
        self.webView().openUrlInNewTab(url, Falkon.Qz.NT_NotSelectedTab)

    def on_act_shift_triggered(self, url):
        Falkon.MainApplication.instance().createWindow(Falkon.Qz.BW_NewWindow, url)

    def on_add_action(self, itm_url, menu):
        act = Falkon.Action(parent=menu)
        title = QtGui.QFontMetrics(act.font()).elidedText(itm_url.title(), QtCore.Qt.ElideRight, 300)
        act.setText(title)
        act.setIcon(itm_url.icon())
        act.triggered.connect(lambda b: self.on_act_triggered(itm_url.url()))
        act.ctrlTriggered.connect(lambda: self.on_act_ctrl_triggered(itm_url.url()))
        act.shiftTriggered.connect(lambda: self.on_act_shift_triggered(itm_url.url()))
        menu.addAction(act)

    def on_add_folder(self, folder, menu):
        sub_menu = Falkon.Menu(parent=menu)
        sub_menu.setCloseOnMiddleClick(True)
        sub_menu.setIcon(folder.icon())
        sub_menu.setTitle(folder.title())
        for itm in folder.children():
            if itm.isUrl():
                self.on_add_action(itm, sub_menu)
            elif itm.isFolder():
                self.on_add_folder(itm, sub_menu)
            elif itm.isSeparator():
                sub_menu.addSeparator()
        menu.addMenu(sub_menu)
        if sub_menu.isEmpty():
            sub_menu.setIcon(QtGui.QIcon.fromTheme("action-unavailable"))
            sub_menu.addAction("Empty").setDisabled(True)

    def on_build_menu(self, folder, menu):
        for itm in folder.children():
            if itm.isUrl():
                self.on_add_action(itm, menu)
            elif itm.isFolder():
                self.on_add_folder(itm, menu)
            elif itm.isSeparator():
                menu.addSeparator()

    def on_clicked(self, controller):
        bookmarks = Falkon.MainApplication.instance().bookmarks()
        menu = Falkon.Menu(parent=controller.visualParent)
        menu.setCloseOnMiddleClick(True)
        self.on_add_folder(bookmarks.toolbarFolder(), menu)
        self.on_add_folder(bookmarks.unsortedFolder(), menu)
        self.on_build_menu(bookmarks.menuFolder(), menu)
        menu.popup(controller.callPopupPosition(menu.sizeHint()))
        menu.aboutToHide.connect(controller.callPopupClosed)

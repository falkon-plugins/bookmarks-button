# ============================================================
# Bookmarks Button extension for Falkon
# Copyright (C) 2019 Zdravko Mitov <mitovz@mail.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ============================================================
import Falkon
from PySide2 import QtCore
from bookmarksbutton.bookmarks_button import BookmarksButton


class BookmarksButtonPlugin(Falkon.PluginInterface, QtCore.QObject):
    buttons = {}

    def init(self, state, settingsPath):

        plugins = Falkon.MainApplication.instance().plugins()
        plugins.mainWindowCreated.connect(self.on_window_created)
        plugins.mainWindowDeleted.connect(self.on_window_deleted)

        if state == Falkon.PluginInterface.LateInitState:
            for window in Falkon.MainApplication.instance().windows():
                self.on_window_created(window)

    def unload(self):
        for window in Falkon.MainApplication.instance().windows():
            self.on_window_deleted(window)

    def testPlugin(self):
        return True

    def on_window_created(self, window):
        b = BookmarksButton()
        window.navigationBar().addToolButton(b)
        self.buttons[window] = b

    def on_window_deleted(self, window):
        if window not in self.buttons:
            return
        b = self.buttons[window]
        window.navigationBar().removeToolButton(b)
        del self.buttons[window]


Falkon.registerPlugin(BookmarksButtonPlugin())

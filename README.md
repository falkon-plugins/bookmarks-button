### A tool-bar bookmarks button for [Falkon](https://phabricator.kde.org/source/falkon/) web browser.

Requested at https://bugs.kde.org/show_bug.cgi?id=391916

### Note:
The files listed here are not official part of **Falkon**, so you should not refer any related issue there!
